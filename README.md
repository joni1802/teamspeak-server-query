# Teamspeak Server Query

A simple and lightweight server query for a teamspeak 3 server

```javascript
const serverquery = require('teamspeak-server-query')
const ts3 = new serverquery()

ts3.connect({host: '127.0.0.1', port: '10011'})
  .then(() => ts3.login({client_login_name: 'serveradmin', client_login_password: 'XXX'}))
  .then(() => ts3.use({sid: 1}))
  .then(() => ts3.clientdblist())
  .then(response => {
    let clients = response

    console.log(clients)
    /*
      [ { cldbid: '2',
      client_unique_identifier: 'odJYAh+ZlPblMwy6RebOCSg9/y0=',
      client_nickname: 'TeamSpeakUser',
      client_created: '1555355453',
      client_lastconnected: '1556716574',
      client_totalconnections: '20',
      client_description: null,
      client_lastip: '10.10.10.30',
      client_login_name: null },
    { cldbid: '3',
      client_unique_identifier: 'ServerQuery',
      client_nickname: 'ServerQuery Guest',
      client_created: '1555355453',
      client_lastconnected: '1555355453',
      client_totalconnections: '0',
      client_description: null,
      client_lastip: null,
      client_login_name: null },
    { cldbid: '4',
      client_unique_identifier: 'GSgJBYEL0YD9yd3w3PxM2TkYH4Y=',
      client_nickname: 'Gast01',
      client_created: '1556314829',
      client_lastconnected: '1556314829',
      client_totalconnections: '1',
      client_description: null,
      client_lastip: '10.10.10.30',
      client_login_name: null } ]
     */
  })
  .then(() => ts3.quit())
  .catch(err => console.log(err))
```

## Installing

Installation is done using the npm install command:

```
$ npm install teamspeak-server-query
```

## How it works

All available query commands can be called as a method. The paramters are represented as an object.

```javascript
ts3.servergroupadd({name: 'Super Bros'}) // => "servergroupadd name=Super\sBros"
```

The methods always return the first response from the server query as an array.

```javascript
let response = await ts3.servergroupadd({name: 'SuperBros'})

console.log(response) // [ { sgid: '20' } ]
```

```javascript
let response = await ts3.servergrouprename({
  sgid: 21,
  name: 'My New Awesome Name'
})

console.log(response) // [ { error: null, id: '0', msg: 'ok' } ]

```

If the query returns an error, the response contains the error. Simple as that.

```javascript
await ts3.servergroupadd({name: 'Buddies'})
let response = await ts3.servergroupadd({name: 'Buddies'})

console.log(response) // [ { error: null, id: '1282', msg: 'database duplicate entry' } ]
```

## Event Listeners

| Name          | Description                                                                                                              |
| ------------- |--------------------------------------------------------------------------------------------------------------------------|
| 'close'       | Emitted when the connection to server gets closed.                                                                       |
| 'notify'      | Emitted when a server notification is send by the server.                                                                |

## Examples

### Using Async Await

```javascript
const serverquery = require('teamspeak-server-query')
const ts3 = new serverquery()

let params = {
  host: '127.0.0.1',
  port: 10011
}

let queryStuff = async () => {
  try {
    await ts3.connect(params)

    await ts3.login({
      client_login_name: 'serveradmin',
      client_login_password: 'XYZ'
    })

    await ts3.use({
      sid: 1
    })

    let clients = await ts3.clientdblist()

    console.log(clients)

    await ts3.quit()
  } catch(err) {
    console.log(err)
  }
}

queryStuff()
```

### Listen on server notifications

```javascript
ts3.on('notify', data => {
  console.log(data)
})

await ts3.servernotifyregister({
  id: 0,
  event: 'channel'
})
```

### Quit the Connection

... as you would do it in the telnet session.

```javascript
ts3.quit()
```

## Built With

* my hands
* [net.Socket](https://nodejs.org/api/net.html#net_class_net_socket)

## Authors

* **Jonathan Francke**
