const serverquery = require('../teamspeak-server-query')
const ts3 = new serverquery({logging: true})

let params = {
  host: '127.0.0.1',
  port: 10011
}

let queryStuff = async () => {
  try {
    await ts3.connect(params)

    await ts3.login({
      client_login_name: 'serveradmin',
      client_login_password: 'XYZ'
    })

    await ts3.use({
      sid: 1
    })

    let clientdblist = await ts3.clientdblist()

    console.log(clientdblist);

    await ts3.quit()
  } catch(err) {
    console.log(err);
  }
}

queryStuff()
