const serverquery = require('../teamspeak-server-query')
const ts3 = new serverquery()

let params = {
  host: '127.0.0.1',
  port: 10011
}

ts3.on('notify', data => {
  console.log(data)
})

let queryStuff = async () => {
  try {
    await ts3.connect(params)

    await ts3.login({
      client_login_name: 'serveradmin',
      client_login_password: 'XYZ'
    })

    await ts3.use({
      sid: 1
    })

    await ts3.servernotifyregister({
      id: 0,
      event: 'channel'
    })



  } catch(err) {
    console.log(err);
  }
}

queryStuff()
